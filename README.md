# NEIApplicationSecurity

[![CI Status](http://img.shields.io/travis/James Whitfield/NEIApplicationSecurity.svg?style=flat)](https://travis-ci.org/James Whitfield/NEIApplicationSecurity)
[![Version](https://img.shields.io/cocoapods/v/NEIApplicationSecurity.svg?style=flat)](http://cocoapods.org/pods/NEIApplicationSecurity)
[![License](https://img.shields.io/cocoapods/l/NEIApplicationSecurity.svg?style=flat)](http://cocoapods.org/pods/NEIApplicationSecurity)
[![Platform](https://img.shields.io/cocoapods/p/NEIApplicationSecurity.svg?style=flat)](http://cocoapods.org/pods/NEIApplicationSecurity)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

NEIApplicationSecurity is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "NEIApplicationSecurity"
```

## Author

James Whitfield, jwhitfield@neilab.com

## License

NEIApplicationSecurity is available under the MIT license. See the LICENSE file for more info.
