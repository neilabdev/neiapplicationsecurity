//
//  NEIApplicationSecurity.h
//  Pods
//
//  Created by NEiLAB, LLC on 10/30/16.
//
//

#import <Foundation/Foundation.h>

typedef void (^NEIGenericBlock) (BOOL success, id result);

@interface NEIApplicationSecurity : NSObject
+ (NEIApplicationSecurity *)instance;


@end
