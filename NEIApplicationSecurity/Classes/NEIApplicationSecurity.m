//
//  NEIApplicationSecurity.m
//  Pods
//
//  Created by NEiLAB, LLC on 10/30/16.
//
//

#import "NEIApplicationSecurity.h"

@implementation NEIApplicationSecurity

// initialization

+ (NEIApplicationSecurity *)instance {
    static NEIApplicationSecurity *_instance = nil;

    @synchronized (self) {
        if (_instance == nil) {
            _instance = [[self alloc] init];
        }
    }

    return _instance;
}

- (void) initializeThen: (NEIGenericBlock) block {
    
}

// registration


// authentication



@end
