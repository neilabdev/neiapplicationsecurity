//
//  main.m
//  NEIApplicationSecurity
//
//  Created by James Whitfield on 10/30/2016.
//  Copyright (c) 2016 James Whitfield. All rights reserved.
//

@import UIKit;
#import "NEIAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NEIAppDelegate class]));
    }
}
